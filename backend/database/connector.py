# Database connector api
# Purpose: To initialize a mysql connection using environment variables passed in from .env file
# and define convenience methods for commonly used database interactions for our backend server.
# Based on Connector.py in kyblit 2020summer main repo
# Author: Michael Ruiz : michael@mruiz.dev
import mysql.connector
from mysql.connector import Error
from mysql.connector import pooling
import sys
import json
import uuid

# A note on keys: Keys will be sent/received in the readable format to user.
# Here, they will be stored without strings as a pure hex value. Use convenience methods to convert before returning val
# More reading here : https://docs.python.org/3/library/uuid.html


def _to_str(key_hex: str) -> str:
    return str(uuid.UUID(key_hex))


def _to_hex(key_str: str) -> str:
    return uuid.UUID(key_str).hex


def _to_uuid(key_str: str) -> uuid.UUID:
    return uuid.UUID(key_str)


class ApiConnector:
    def __init__(self, envData: dict):
        self.env = envData
        self.conn = self._connect()
    # Private members
    # auto connects on init, no need to call this method outside of class
    def _connect(self):
        db_host = self.env['DB_HOST']
        db_name = self.env['DB_NAME']
        db_port = self.env['DB_PORT']
        db_user = self.env['DB_USER']
        db_pass = self.env['DB_PASS']
        db_plugin = self.env['DB_PLUGIN']
        db_type = self.env['DB_TYPE']           # mariadb or mysql
        secret = self.env['SECRET_KEY']         # api secret
        try:
            pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="apiPool",
                                                               pool_size=1,
                                                               pool_reset_session=True,
                                                               host=db_host,
                                                               port=db_port,
                                                               database=db_name,
                                                               user=db_user,
                                                               password=db_pass,
                                                               auth_plugin=db_plugin)
            return pool.get_connection()
        except Error as e:
            print(e)
            sys.exit(43)

    # Public methods
    def get_connection(self):
        """:returns the connection object."""
        return self.conn

    # CREATE/INSERT METHODS
    def create_api_key(self, name, email) -> bool:
        """Creates a new api key if the user's email is not found.
        :argument name: The new api keyholder's name
        :argument email: Keyholders email
        :returns boolean"""
        key = uuid.uuid4()
        if not self.get_api_key(email):
            sql = f"INSERT INTO ky_auth_token(token, name, email)" \
                  f" VALUES('{key.hex}', '{name}', '{email}')"
            cur = self.conn.cursor()
            cur.execute(sql)
            self.conn.commit()
            return True
        else:
            return False

    # RETRIEVE METHODS
    def get_api_key(self, email: str):
        """Gets the api key by user's email.
        :returns Api UUID string if found or None"""
        sql = f'''SELECT token FROM ky_auth_token WHERE email = '{email}' '''
        cur = self.conn.cursor()
        cur.execute(sql)
        result = cur.fetchone()
        if result:
            return _to_str(result[0])
        else:
            return None

    def get_related_words(self, query_word: str) -> str:
        """
        Get a json string of related words based on query word
        :argument query_word The word requested to search
        :returns A json string of matching related words"""
        cursor = self.conn.cursor()
        sql = f"SELECT name, id FROM ky_object a WHERE (a.id IN " \
              f"(SELECT result FROM ky_object_relation b WHERE b.lookup_id = " \
              f"(SELECT id FROM ky_object WHERE name = '{query_word}')));"
        cursor.execute(sql)
        records = cursor.fetchall()
        return json.dumps(records)

    def is_request_allowed(self, key: str) -> int:
        """
        Checks users requests limit to allow request through or not.
        :param key: User's uuid key
        :return: Returns response code
        """
        token = _to_hex(key)
        cursor = self.conn.cursor()
        sql = f"""SELECT requests_sent, requests_limit FROM ky_auth_token WHERE token = '{token}' """
        cursor.execute(sql)
        result = cursor.fetchone()
        if result is None:
            return 404      # return not found
        sent, limit = result
        if sent >= limit:
            return 403      # return forbidden
        return 200  # OK
    # UPDATE METHODS
    def increment_api_call(self, key: str):
        """
        Increments requests sent column on each api call
        :param key: uuid key in the human readable format with dashes
        :return: Returns true on update
        """
        token = _to_hex(key)
        cursor = self.conn.cursor()
        sql = f"""UPDATE ky_auth_token SET requests_sent = requests_sent + 1 WHERE token = '{token}' """
        cursor.execute(sql)
        self.conn.commit()
        return True
        # TODO: add try/except
    # DELETE METHODS

