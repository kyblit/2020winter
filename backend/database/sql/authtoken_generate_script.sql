-- CREATE DATABASE IF NOT EXISTS 2020summer;
USE 2020summer;
DROP TABLE IF EXISTS `ky_auth_token`;
DROP TABLE IF EXISTS `ky_version`;

CREATE TABLE `ky_auth_token` (
    `token` VARCHAR(32) NOT NULL PRIMARY KEY UNIQUE,
    `email` VARCHAR(64) NOT NULL,
    `name` VARCHAR(32) NOT NULL,
    `requests_sent` INTEGER DEFAULT 0,
    `requests_limit` INTEGER DEFAULT 500,
    unique key (`email`));

CREATE TABLE `ky_version` (
  `version` VARCHAR(16) NOT NULL);

INSERT INTO `ky_version`
VALUES ('1.0.1');