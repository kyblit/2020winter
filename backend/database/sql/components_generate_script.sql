-- CREATE DATABASE IF NOT EXISTS 2020summer;

-- ky_component
--
--id int
--
--component_name
--name varchar(100)
--description varchar(500)
--created_date timestamp
--primary_key (id)


--ky_component_request
--id
--id int
--component_id int
--request_type varchar(100)
--description varchar(500)
--created_date timestamp
--primary_key (id)

--1, 'anagram', 'generated a list of words with a given set of letters.  Usage: describe how it can be use ...
--that goes into ky_component table
--
--in the ky_component_request table, you can have
--1, 1, 'getAnagramWords', 'get a list of anagram words with the following paramters..'
--1, 2, 'AnyPanagram', 'check to see if a list of letters can generate one or more panagram words'
--
--https://<our URL/<component name>/<component request>

--in the ky_component_request table
--we may need to column such as 'type' to define if the request is a 'GET', 'POST', 'PUT', etc
--decide get and post part
USE 2020summer;
DROP TABLE IF EXISTS `ky_auth_token`;
DROP TABLE IF EXISTS `ky_version`;

CREATE TABLE `ky_component` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL,
    `description` varchar(500),
    `created_date` timestamp,
     PRIMARY KEY (`id`),
     unique key (`name`));

CREATE TABLE `ky_component_request` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `component_id` int,
    `name` varchar(100),
    `type` varchar(100),
    `description` varchar(500),
    `created_date` timestamp,
    `modified_date` timestamp,
     PRIMARY KEY (`id`),
     unique key (`id`),
     unique key (`component_id`),
     unique key (`name`));


INSERT INTO `ky_version`
VALUES ('1.0.1');