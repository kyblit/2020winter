from os import path
from bisect import bisect_left
from itertools import combinations
from itertools import combinations_with_replacement
import sys
# Source of words: SOWPODS
# https://www.wordgamedictionary.com/twl06/download/twl06.txt

# Based off Justin Peel's Stackoverflow answer
# on "How can this Python Scrabble word finder be made faster?":
# https://stackoverflow.com/a/5521619


class WordLookup:
    def __init__(self):
        self.generate_sorted_words_file()
        self.sorted_words_list = self.load_sorted_words_from_file()
        self.max_word_len = self.calc_max_word_len()

    # Step 1:
    # Store all words from the word list of all words into a dictionary
    # where the key is the is the sorted version of the letters in the word.
    # The value is the list of all words that become the key when sorted
    # (in other words, are anagrams of the sorted key).
    # This dictionary is placed into a text file, sorted_words_list.txt

    def generate_sorted_words_file(self):
        if not path.exists("sorted_words_list.txt"):
            try:
                word_file = open("word_list.txt", "r")
                words_dict = {}
                for word in word_file:
                    word = word.strip()
                    sorted_word = "".join(sorted(word))
                    if sorted_word in words_dict:
                        words_dict[sorted_word].append(word)
                    else:
                        words_dict[sorted_word] = [word]
                word_file.close()
            except FileNotFoundError:
                print("missing words list")
                sys.exit(-1)
            sorted_words_list = [' '.join([key] + value) for key, value in words_dict.items()]
            sorted_words_list.sort()
            f = open("sorted_words_list.txt", "w")
            f.write("\n".join(sorted_words_list))
            f.close()

    # Step 2:
    # Load sorted_words_list.txt into a list
    # with each line in the file as an element of the list.
    # Example of an element of the list:
    # 'inopt pinot pinto piton point'

    def load_sorted_words_from_file(self):
        try:
            f = open("sorted_words_list.txt", "r")
            sorted_words_list = f.read().split("\n")
            f.close()
            return sorted_words_list
        except FileNotFoundError:
            print("Missing  sorted words list")
            sys.exit(-1)

    def calc_max_word_len(self):
        longest_word_line = max(self.sorted_words_list, key=lambda x: len(x.split(" ")[0]))
        longest_len = len(longest_word_line.split(" ")[0])
        return longest_len

    # Step 3 find words
    def find_words(self, letters, required_letters, min_letters, letters_can_be_repeated, required_letters_ordered,
                   sort_alpha):
        letters = letters.lower()
        letters = "".join(list(set(letters))) # remove duplicates by converting to set and reconverting to string
        letters = sorted(letters.lower())
        anagrams = []

        # max_letters = len(letters) + 2
        if not letters_can_be_repeated:
            max_letters = len(letters)
        else:
            max_letters = self.max_word_len

        for i in range(max_letters, min_letters-1, -1):
        # for i in range(min_letters, max_letters + 1):
            possible_words = []
            if letters_can_be_repeated:
                possible_words = combinations_with_replacement(letters, i)
            else:
                possible_words = combinations(letters, i)
            for combination in possible_words:
                possible_anagram = "".join(combination)
                found_letters_index = bisect_left(self.sorted_words_list, possible_anagram)
                # Binary sort with bisect_left to find the key in the text file that
                # matches the sorted version of the word

                # bisect finds the appropriate index to insert the word,
                # so the words after the spaces after the first word are not an issue
                # https://www.geeksforgeeks.org/bisect-algorithm-functions-in-python/
                if found_letters_index == len(self.sorted_words_list):
                    continue

                words = self.sorted_words_list[found_letters_index].split()
                if words[0] == possible_anagram:
                    for word in words[1:]:
                        if not word in anagrams: # avoid duplicates
                            anagrams.append(word)


        # filter words based on the required letters
        if required_letters != "":
            if required_letters_ordered:
                filtered_word_list = []
                for word in anagrams:
                    prev_index = 0
                    valid_word = True
                    for letter in required_letters:
                        new_index = word.find(letter, prev_index)
                        if new_index == -1:
                            valid_word = False
                            break
                        prev_index = new_index
                    if valid_word:
                        filtered_word_list.append(word)
                anagrams = filtered_word_list

            else:
                anagrams = [word for word in anagrams if (set(required_letters) & set(word))]

        # sort, if sort_alpha is True then sort alphabetically,
        # otherwise sort from highest to lowest length (default).

        if sort_alpha:
            anagrams.sort()

        for item in anagrams:
            print(item)

        # json format
        anagram_dict = {"".join(letters): anagrams}
        return anagram_dict



word_lookup = WordLookup()
# letters, required_letters, min_letters, letters_can_be_repeated, required_letters_ordered,
# sort_alpha)
print(word_lookup.find_words("mpnlaor", "n", 3, True, False, False))

