# add service to this file or create new files in this folder
from backend.model.model import Home


class HomeService:
    def __init__(self):
        self.model = Home
        self.page = Home()

    def get_text(self):
        return self.page.get_text()
