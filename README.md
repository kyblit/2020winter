## Getting Started
* First you should rename the `sample.env` file to `.env` and customize the variables.
* Run `pip install -r requirements.txt` or manually install the dependencies listed there.
* Run `python application.py <path to .env file>` from command line or IDE to run flask server.


*Note:* Windows pip command may differ. `python -m pip install -r requirements.txt`


## Webpage
Navigate to `localhost:5000/api?q=<desired word>` to view a list of related words.


*Webpage is not fully functional at this time.*

## API
Alternatively, use the following command from command line to use API: 


`curl localhost:5000/api?q=<desired word>`

