from flask import Flask, render_template, url_for, request, session, redirect
from os import urandom
from backend.envparser import EnvParser
from backend.service.service import HomeService
from backend.database.connector import ApiConnector
import json
import sys
import uuid


if(len(sys.argv) > 1):
    path = sys.argv[1]
else:
    print("Please enter the path to your .env file as the first argument")
    sys.exit(100)
envParse = EnvParser(path)
env = envParse.get_variables()                      # get variables from .env file
db = ApiConnector(env)
application = Flask(__name__, template_folder='./frontend/templates')
application.secret_key = env['SECRET_KEY']          # unused attr


@application.route("/")
def home():
    page_text = HomeService().get_text()
    return render_template('index.html', active='home', page_text=page_text)


@application.route("/register", methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        page_text = HomeService().get_text()
        return render_template('reg.html', active='register', page_text=page_text)

    elif request.method == 'POST':
        req = request.form
        name = req['name']
        email = req['email']
        if db.create_api_key(name, email):
            result = db.get_api_key(email)
            return result
        else:
            return "ERROR Email exists/some unhandled error"


@application.route("/api/related/<key>/<word>", methods=['GET'])
def api(key, word):
    if key:
        response_code = db.is_request_allowed(key)
        if response_code == 200:
            db.increment_api_call(key)
        else:
            return f"ERROR {response_code}"
    if word:
        json_str = db.get_related_words(word)
        return json_str
    return "Empty query"


@application.route("/findword/echo", methods=['GET'])
def echo():
    data = request
    if data.args['q']:
        q = data.args['q']
        return q
    return "Empty query"


@application.route("/<page>")
def undefined(page):
    return render_template("undefined.html", page=page)


def run():
    application.run(debug=True)


if __name__ == '__main__':
    run()
